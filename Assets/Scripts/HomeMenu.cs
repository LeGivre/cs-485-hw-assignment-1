﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HomeMenu : MonoBehaviour
{

    public void Button1Pressed()
    {
        SceneManager.LoadScene("RollABall", LoadSceneMode.Single);
    }

    public void Button2Pressed()
    {
        SceneManager.LoadScene("SpaceshipDrift", LoadSceneMode.Single);
    }

    public void Button3Pressed()
    {
        Application.Quit();
    }
}
