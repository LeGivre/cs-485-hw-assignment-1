﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SpaceshipController : MonoBehaviour
{
    public float speedForce;
    public Text firstLapTime;
    public Text secondLapTime;
    public Text thirdLapTime;
    public Text winText;

    private int lapCount = -1;
    private Rigidbody rb;
    private float startTime = 0f;
    private string time;
    void Start()
    {
        winText.text = "";
        rb = GetComponent<Rigidbody>();
        speedForce = 25f;
    }

    void FixedUpdate()
    {
        time = (Time.time - startTime).ToString();
        if (Input.GetKey(KeyCode.UpArrow))
            rb.AddForce(transform.forward * speedForce);
        if (Input.GetKey(KeyCode.Space))
            rb.drag += 0.1f;
        if (!Input.GetKey(KeyCode.Space))
            rb.drag = 0.5f;

        transform.Rotate(0 , Input.GetAxis("Horizontal") * Time.deltaTime * 100f, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Reseter"))
        {
            lapCount++;
            if (lapCount == 1)
                firstLapTime.text += time;
            else if (lapCount == 2)
                secondLapTime.text += time;
            else if (lapCount == 3)
            {
                thirdLapTime.text += time;
                winText.text = "You have finish!!!";
            }
            startTime = Time.time;
        }
    }
}
