﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject PauseMenu;
    public AudioSource music;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
                Resume();
            else
                Pause();
        }
    }

    public void Resume()
    {
        PauseMenu.SetActive(false);
        music.Play();
        GameIsPaused = false;
        Time.timeScale = 1f;
    }

    void Pause()
    {
        PauseMenu.SetActive(true);
        music.Pause();
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void menu()
    {
        SceneManager.LoadScene("HomeMenu");
        GameIsPaused = false;
        Time.timeScale = 1f;
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
